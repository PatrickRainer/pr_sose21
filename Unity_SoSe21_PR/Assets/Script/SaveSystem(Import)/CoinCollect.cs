using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;

namespace PTWO_PR
{
    public class CoinCollect : MonoBehaviour
    {

                       
       //By collision with the player object: the coin instance gets destroyed and the Score increases by one
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Player")
            {
                CoinScore.Score += 1;

                

                Destroy(gameObject);


            }
        }
    }
}

