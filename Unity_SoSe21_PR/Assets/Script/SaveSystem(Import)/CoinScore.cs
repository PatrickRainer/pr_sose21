using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;

namespace PTWO_PR
{
    public class CoinScore : MonoBehaviour
    {
        //the score value which is displayed 
        [SerializeField]
        public static int Score = 0;

        //the display of the score in the top left corner
        [SerializeField]
        private TextMeshProUGUI scoreCount;

        
        public static bool CoinExists = true;
        public static bool CoinOneExists = true;
        public static bool CoinTwoExists = true;
        public static bool CoinThreeExists = true;
        public static bool CoinFourExists = true;
        public static bool CoinFiveExists = true;

        
        public static bool CoinsLoaded = false;

        [SerializeField]
        private GameObject[] AllCoins = new GameObject[6];

        


        private void Update()
        {
            //prints the score to the textMesh at the top left
            scoreCount.text = Score.ToString();

            //each checks if the instance(game object) of the coin currently exists

            if (AllCoins[0] == null)
            {
                CoinExists = false;
            }

            if (AllCoins[1] == null)
            {
                CoinOneExists = false;
            }

            if (AllCoins[2] == null)
            {
                CoinTwoExists = false;
            }

            if (AllCoins[3] == null)
            {
                CoinThreeExists = false;
            }

            if (AllCoins[4] == null)
            {
                CoinFourExists = false;
            }

            if (AllCoins[5] == null)
            {
                CoinFiveExists = false;
            }

            //checks if the player loaded the scene and therfore his score 
            if(CoinsLoaded == true)
            {
                //each checks if the respected coin was collected and should therefore not exist in the loaded scene 
                if (CoinScore.CoinExists == false)
                {
                    Destroy(AllCoins[0]);
                }

                if (CoinScore.CoinOneExists == false)
                {
                    Destroy(AllCoins[1]);
                }

                if (CoinScore.CoinTwoExists == false)
                {
                    Destroy(AllCoins[2]);
                }

                if (CoinScore.CoinThreeExists == false)
                {
                    Destroy(AllCoins[3]);
                }

                if (CoinScore.CoinFourExists == false)
                {
                    Destroy(AllCoins[4]);
                }

                if (CoinScore.CoinFiveExists == false)
                {
                    Destroy(AllCoins[5]);
                }

                CoinsLoaded = false;
            }
        }



    }

}
