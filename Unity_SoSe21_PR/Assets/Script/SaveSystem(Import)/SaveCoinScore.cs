using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PTWO_PR
{
    public class SaveCoinScore : MonoBehaviour
    {
        
        public static string SCORE_KEY = "SCORE";


        
        
        //will save the current coin score when the function is called/the button is pressed
        public void saveButtonClick()
        {
            
            PlayerPrefs.SetInt(SCORE_KEY, CoinScore.Score);
            Debug.Log("Your Score is: " + PlayerPrefs.GetInt(SCORE_KEY));


        }

        //returns the value of the last saved coin score
        public static int LoadScore()
        {
            return PlayerPrefs.GetInt(SCORE_KEY);
        }

        
    }

}
