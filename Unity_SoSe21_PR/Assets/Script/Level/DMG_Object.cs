using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTWO_PR
{
    public class DMG_Object : MonoBehaviour
    {
        [SerializeField]
        private GameObject player;

        //checks if the player hits an object that can damage them
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Player")
            {
                //sends the player to the Game Over Screen

                GameOver.playerLives = false;

                
                
            }
        }


    }
}

