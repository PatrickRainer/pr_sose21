using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PTWO_PR
{
    public class FinishGame : MonoBehaviour
    {
        [SerializeField]
        private string sceneName;

        //checks if the player reaches the flag 
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Player")
            {
                //checks if the player has enough coins to finish the game
                if(CoinScore.Score == 6)
                {
                    LoadScene();
                }
               

            }

        }

        public void LoadScene()
        {
            SceneManager.LoadScene(sceneName);
        }

    }
}

