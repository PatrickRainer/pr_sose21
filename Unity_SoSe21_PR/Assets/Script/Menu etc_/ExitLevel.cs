using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PTWO_PR
{
    public class ExitLevel : MonoBehaviour
    {
        [SerializeField]
        private string sceneName;

        public void Update()
        {
            //checks if the player pressed esc and leads them to the main menu
            if (Input.GetKey("escape"))
            {
                SceneManager.LoadScene(sceneName);
                CoinScore.Score = 0;
            }

        }
    }

}

