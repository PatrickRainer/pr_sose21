using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PTWO_PR
{
    public class SceneLoading : MonoBehaviour
    {
        [SerializeField]
        private string sceneName;

        [SerializeField]
        private string MainMenu;

        

        

        

        //loads the first level and rests the score
        public void LoadScene()
        {
            SceneManager.LoadScene(sceneName);
            CoinScore.Score = 0;
        }

        //loads the level while also remembering the score of the player; notifics CoinScore that the score has been loaded
        public void LoadSceneSave()
        {
            SceneManager.LoadScene(sceneName);
            CoinScore.Score = SaveCoinScore.LoadScore();

            CoinScore.CoinsLoaded = true;

        }

        //loads the main menu
        public void LoadSceneMM()
        {
            SceneManager.LoadScene(MainMenu);
        }

        //quits the game
        public void QuitGame()
        {
            Application.Quit();
        }
        

    }

}
