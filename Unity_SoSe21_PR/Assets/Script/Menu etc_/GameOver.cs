using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PTWO_PR
{
    public class GameOver : MonoBehaviour
    {
        public static bool playerLives = true;

        [SerializeField]
        private string sceneName;

        
        private void Update()
        {
            //checks if the player is still alive and if not delets the score, loads the GameOverScene and destroys the player instance
            if(playerLives == false)
            {
                LoadScene();
                CoinScore.Score = 0;

                Destroy(gameObject);

                playerLives = true;
            }
        }

        public void LoadScene()
        {
            SceneManager.LoadScene(sceneName);
        }


    }
}

