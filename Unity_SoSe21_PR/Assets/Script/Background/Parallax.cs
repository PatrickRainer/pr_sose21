using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTWO_PR
{
    public class Parallax : MonoBehaviour
    {
        //camera reference
        public Camera cam;

        //player reference
        public Transform player;

        
        private Vector2 startPosition;

        private float startZ;

        private float startY;

        private Vector2 travel => (Vector2)player.transform.position - startPosition;

        //measures the distance between player object and background
        private float distanceFromPlayer => transform.position.z - player.position.z;
        // measures  clipping Plane how far away the camera is 
        private float clippingPlane => (cam.transform.position.z + (distanceFromPlayer > 0 ? cam.farClipPlane : cam.nearClipPlane));

        private float parallaxFactor => Mathf.Abs(distanceFromPlayer) / clippingPlane;
        
        
        private void Start()
        {
            startPosition = transform.position;

            startZ = transform.position.z;

            startY = transform.position.y;
        }

        private void Update()
        {
            Vector2 newPosition = transform.position = startPosition + travel * parallaxFactor;

            transform.position = new Vector3(newPosition.x,startY, startZ);


        }
        
        
    }
}

